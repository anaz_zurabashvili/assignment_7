package com.example.assignment_7.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.view.View
import com.example.assignment_7.R
import com.example.assignment_7.databinding.ActivityMainBinding
import com.example.assignment_7.extensions.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.signInBtn.setOnClickListener(saveBtnClick)
        binding.password.setOnLongClickListener(passwordIconClick)

    }

    private fun isAllFieldsValid(): Boolean {
        return !(binding.emailAddress.text.isNullOrBlank() ||
                binding.password.text.isNullOrBlank())
    }

    private fun makeValidation(): Boolean = isAllFieldsValid() &&
            binding.emailAddress.text.toString().emailValid()

    private val saveBtnClick = View.OnClickListener {
        if (makeValidation()) {
            binding.infoMessage.text = getString(R.string.success_message)
            binding.infoMessage.visible()
        } else {
            binding.infoMessage.text = getString(R.string.error_message)
            binding.infoMessage.visible()
        }
    }
    private val passwordIconClick = View.OnLongClickListener {
        // InputType.TYPE_TEXT_VARIATION_PASSWORD is 128 and it does not work
        //
        if (binding.password.inputType == 129) {
            binding.password.inputType = InputType.TYPE_CLASS_TEXT
        } else {
            binding.password.inputType = 129
        }
        true
    }
}