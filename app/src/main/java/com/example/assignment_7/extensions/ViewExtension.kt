package com.example.assignment_7.extensions

import android.view.View

fun View.gone() = View.GONE.also { visibility = it }

fun View.visible() = View.VISIBLE.also { visibility = it }
